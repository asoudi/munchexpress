<?php

namespace App\Services;

use App\Models\Menu;

class MenuService {

    public function getMenusWithCategoriesByRest(array $restId){

        return Menu::whereIn('resto_id', $restId)
        ->get()
        ->groupby('category.name');
    }

}