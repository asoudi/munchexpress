<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    // Load Category with the Model Menu by default { Eager Loading }
    protected $with = ['Category'];

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }
}
