@extends('layouts.app')

@section('content')
    <div class="container">
       <div class="row">
           <div class="col-md-12">
               <h3>Order Managed by Resturant {{ $resto->name }}</h3>
           </div>
       </div>
    @if($orders->count() > 0 )
        <div class="row mb-3">
            <div class="col-md-12 ">
                <table class="table table-hover table-striped table-bordered ">
                    <thead>
                        <tr>
                            <td>Order Id</td>
                            <td>Amount</td>
                            <td>Status</td>
                            <td>Customer Details</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->amount}}</td>
                                <td>{{($order->isCompleted) ? 'Completed' : 'Incompleted'}}</td>
                                <td>
                                    Name: {{ $order['order_details']['customer_name'] }}
                                    <br>
                                    Address: {{ $order['order_details']['customer_address'] }}
                                    <br>
                                    Phone: {{ $order['order_details']['customer_phone'] }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{$orders->render()}}
        @else
            <p>You don't have orders yet.</p>
    @endif
    </div>
@endsection
